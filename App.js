import React from 'react';
import Routes from './src/routes/Routes';
import * as Notifications from 'expo-notifications';
import NotificationListener from './src/notifications/NotificationListener';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default function App() {
  return (
    <>
      <NotificationListener />
      <Routes />
    </>
  );
}
