import AsyncStorage from '@react-native-async-storage/async-storage';
import Http from '../config/Http';

const login = async (userData) => {
    return(
        Http({
            method: 'POST',
            url: `/auth/login`,
            data: userData,
        }).then((response) => {
            return response.data.access_token;
        })
    );
}

const create = async (userData) => {
    return(
        Http({
            method: 'POST',
            url: `/user/create`,
            data: userData,
        }).then((response) => {
            return response;
        })
    );
}

const logout = async (userToken) => {

    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }

    let retorno = (
        Http({
            method: 'POST',
            url: `/auth/logout`,
        }).then((response) => {
            return response;
        })
    );

    return retorno;
}

const isThereAnyUser = async () => {
    let userToken = await AsyncStorage.getItem('userToken');
    
    return userToken != 'null';
}

export default {
    login,
    create,
    logout,
    isThereAnyUser
}