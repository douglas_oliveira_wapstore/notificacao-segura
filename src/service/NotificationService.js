import ExpoPushToken from '../config/ExpoPushToken';
import Http from '../config/Http';
import * as Notifications from 'expo-notifications';
import Token from '../config/Token';

const pushActiveNotificationsExpo = async () => {
    let userToken = await Token();

    let response = await list(userToken);

    await Notifications.dismissAllNotificationsAsync();

    response.forEach(element => {
        if(!element.ativo) return;

        showNotification(element.title, element.description, {hash: element.hash, id: element.id});
    });
}

const sendMessage = async (hash) => {
    return(
        Http({
            method: 'POST',
            url: `/notification/send/${hash}`,
            data: {"additional": "Estou localizado nas coordenadas 29 graus sul e 86 graus oeste em Birigui."},
        }).then((response) => {
            return response.data;
        })
    );
}

const create = async (notificationData, userToken) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }
    
    let retornoApi = (
        Http({
            method: 'POST',
            url: `/notification/create`,
            data: notificationData,
        }).then((response) => {
            return response;
        })
    );

    return retornoApi;
}

const list = async (userToken) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`;
    }
    
    return(
        Http({
            method: 'GET',
            url: `/notification/list`,
        }).then((response) => {
            return response?.data;
        })
    );
}

const findById = async ( idNotification, userToken) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`;
    }
    
    return(
        Http({
            method: 'GET',
            url: `/notification/detail/${idNotification}`,
        }).then((response) => {
            return response.data;
        })
    );
}

const edit = async (notificationData, userToken, id) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }
    
    let retornoApi = (
        Http({
            method: 'PUT',
            url: `/notification/edit/${id}`,
            data: notificationData,
        }).then((response) => {
            return response;
        })
    );

    return retornoApi;
}

const on = async (statusData, userToken, id) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }

    let retornoApi = (
        Http({
            method: 'PATCH',
            url: `/notification/edit/${id}`,
            data: statusData,
        }).then((response) => {
            pushActiveNotificationsExpo();

            return response;
        })
    );

    return retornoApi;
}

const exclude = async (userToken, id) => {
    
    if(userToken){
        Http.defaults.headers.common['Authorization'] = `Bearer ${userToken}`
    }
    
    let retornoApi = (
        Http({
            method: 'DELETE',
            url: `/notification/delete/${id}`,
        }).then((response) => {
            pushActiveNotificationsExpo();
            
            return response;
        })
    );
    
    return retornoApi;

}

async function showNotification(title, description, data) {
    let expoPushToken = (await ExpoPushToken()).data;

    Notifications.setNotificationHandler({
        handleNotification: async () => ({
            shouldShowAlert: true,
            shouldPlaySound: false,
            shouldSetBadge: false,
        }),
    });

    const message = {
      to: expoPushToken,
      sound: 'default',
      title: title,
      body: description,
      data: data,
      autoDismiss: true,
      sticky: true
    };
  
    await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
  }

export default {
    create,
    list,
    findById,
    edit,
    on,
    exclude,
    showNotification,
    sendMessage,
    pushActiveNotificationsExpo
}