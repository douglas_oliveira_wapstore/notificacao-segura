import * as React from "react"
import Svg, { Path } from "react-native-svg"
const SvgComponent = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    fill="none"
    viewBox="0 0 25 25"
    {...props}
  >
    <Path
      fill="#DC3131"
      d="M6.667 3.067a2.4 2.4 0 0 1 2.4-2.4h7.2a2.4 2.4 0 0 1 2.4 2.4v2.4h4.8a1.2 1.2 0 0 1 0 2.4h-1.283l-1.04 14.57a2.4 2.4 0 0 1-2.394 2.23H6.583a2.4 2.4 0 0 1-2.394-2.23L3.15 7.867H1.867a1.2 1.2 0 1 1 0-2.4h4.8v-2.4Zm2.4 2.4h7.2v-2.4h-7.2v2.4Zm-3.511 2.4 1.028 14.4H18.75l1.028-14.4H5.556Zm4.71 2.4a1.2 1.2 0 0 1 1.2 1.2v7.2a1.2 1.2 0 0 1-2.4 0v-7.2a1.2 1.2 0 0 1 1.2-1.2Zm4.8 0a1.2 1.2 0 0 1 1.2 1.2v7.2a1.2 1.2 0 0 1-2.4 0v-7.2a1.2 1.2 0 0 1 1.2-1.2Z"
    />
  </Svg>
)
export default SvgComponent
