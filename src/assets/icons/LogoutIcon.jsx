import * as React from "react"
import Svg, { Path } from "react-native-svg"
const SvgComponent = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    fill="none"
    viewBox="0 0 50 50"
    {...props}
  >
    <Path
      fill="#DC3131"
      d="M5.556 50c-1.528 0-2.836-.544-3.923-1.63C.546 47.281.002 45.973 0 44.443V5.556C0 4.028.544 2.72 1.633 1.633 2.723.546 4.03.002 5.556 0H25v5.556H5.556v38.888H25V50H5.556ZM36.11 38.889l-3.82-4.028 7.084-7.083H16.667v-5.556h22.708l-7.083-7.083 3.82-4.028L50 25 36.111 38.889Z"
    />
  </Svg>
)
export default SvgComponent
