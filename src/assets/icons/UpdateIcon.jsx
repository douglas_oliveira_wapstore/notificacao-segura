import * as React from "react"
import Svg, { Path } from "react-native-svg"
const SvgComponent = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    fill="none"
    viewBox="0 0 25 25"
    {...props}
  >
    <Path
      fill="#3C9F9F"
      d="m23.94 2.678-1.452-1.453a1.883 1.883 0 0 0-2.678 0l-6.68 6.68v4.13h4.131l6.68-6.68a1.882 1.882 0 0 0 0-2.677Zm-7.552 7.4h-1.3V8.775l4.358-4.354 1.301 1.301-4.359 4.354Z"
    />
    <Path
      fill="#3C9F9F"
      d="M20.71 22.14H3.026V4.456h7.579V1.93H3.026A2.534 2.534 0 0 0 .5 4.456V22.14a2.534 2.534 0 0 0 2.526 2.526H20.71a2.534 2.534 0 0 0 2.526-2.526V14.56H20.71v7.579Z"
    />
  </Svg>
)
export default SvgComponent
