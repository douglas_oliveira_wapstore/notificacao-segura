import { Platform } from 'react-native';
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';
import GenericAlert from '../messages/GenericAlert';

async function registerForPushNotificationsAsync() {
    let token;
  
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
  
    if (Device.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      console.log('existingStatus dados: ', existingStatus);
      let finalStatus = existingStatus;
      
      while(finalStatus !== 'granted'){
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
        
        if (existingStatus !== 'granted') {
          GenericAlert('Permissão Negada', 'Não podemos te ajudar se não permitir o uso da notificação :(');
          return false;
        }
      }

      if(finalStatus === 'granted'){
        token = await Notifications.getExpoPushTokenAsync({
          projectId: Constants.expoConfig.extra.eas.projectId,
        });
      }
      console.log(token);
    } else {
      GenericAlert('Dispositivo Inválido', 'Use um aparelho físico para ativar a notificação');
    }
  
    return token.data;
  }
  
export default registerForPushNotificationsAsync;