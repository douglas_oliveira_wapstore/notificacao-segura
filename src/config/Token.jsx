import AsyncStorage from '@react-native-async-storage/async-storage';

const Token = async () => {
    try {
        let userToken = await AsyncStorage.getItem('userToken');

        if (userToken !== null) {
          return userToken;
        } else {
          console.log('Token não encontrado');
          return null;
        }
      } catch (error) {
        console.error('Erro ao obter token do AsyncStorage:', error);
      }
}

export default Token;