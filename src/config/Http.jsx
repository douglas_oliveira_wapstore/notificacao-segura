import axios from 'axios';

const Http = axios.create({
    baseURL: 'https://3bcd-187-120-157-68.ngrok-free.app/api',
    headers: {
      Accept: 'application/json'
    },
});

export default Http;