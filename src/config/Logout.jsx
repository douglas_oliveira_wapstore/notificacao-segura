import AsyncStorage from '@react-native-async-storage/async-storage';
import Token from '../config/Token';
import GenericAlert from '../messages/GenericAlert';
import UserService from '../service/UserService';

const Logout = async ({ navigation }) => {
    
    try{
        let userToken = await Token();
        await UserService.logout(userToken);
        await AsyncStorage.removeItem('userToken');
        navigation.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          });
    } catch(error){
        console.error("Não foi possível desconectar o usuário", error);
        GenericAlert("Operação Inválida", "Não foi possível descontar");
    }
}

export default Logout;
