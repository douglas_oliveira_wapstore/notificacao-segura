import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from '../pages/registration/Login';
import Register from '../pages/registration/Register';
import Start from '../pages/registration/Start';
import TabNavigation from './TabNavigation';
import { StatusBar } from 'expo-status-bar';
import { AddContact } from '../pages/loggedPages/CreateNotification/AddContact';
import { EditNotification } from '../pages/loggedPages/EditNotification/Notification';
import { EditContact } from '../pages/loggedPages/EditNotification/EditContact';
import ButtonLogout from '../components/ButtonLogout';
import Logout from '../config/Logout';

const stack = createNativeStackNavigator();

function StackNavigation({navigation}) {

  return (
    <>
      <StatusBar />
        <stack.Navigator 
          screenOptions={({ route }) => ({
          headerTitle: 'Notificação Segura',
          headerTitleAlign: 'center', // Alinha o texto do header no centro
          headerStyle: {
            backgroundColor: '#3C9F9F', // Muda a cor do header
          },
          headerRight: () => {
            if(route.name === 'HomeTabNav'){
              return <ButtonLogout onPress={() => Logout({ navigation })} />;
            }},
          headerTintColor: '#fff', // Muda a cor do texto do header
          headerTitleStyle: {
            fontWeight: 'bold',
            fontFamily: 'Roboto',
            fontSize: 23,
          },
        })}>

          <stack.Screen 
            name='Start' 
            component={Start} 
            options={{ headerShown: false }} // Remove o Header padrão do navigation
          /> 

          <stack.Screen
            name='Login' 
            component={Login}
            options={{headerBackVisible: false}} // Remove a seta de "Voltar página" do header
          />

          <stack.Screen 
            name='Register' 
            component={Register}
            options={{headerBackVisible: false}} 
          />

          <stack.Screen // Acessando o app após o Login e apresentando a Tab Bar
            name='HomeTabNav' 
            component={TabNavigation}
            options={{headerBackVisible: false}} 
          />

          <stack.Screen // Acessando AddContact da notificação
            name='NotificationContact' 
            component={AddContact}
            options={{headerBackVisible: false}} 
          />

          <stack.Screen // Acessando EditNotification da notificação
            name='EditNotification' 
            component={EditNotification}
            options={{headerBackVisible: false}}
          />

          <stack.Screen // Acessando EditContact da notificação
            name='EditContact' 
            component={EditContact}
            options={{headerBackVisible: false}}
          />

        </stack.Navigator>
      </>
  )
}

export default StackNavigation;