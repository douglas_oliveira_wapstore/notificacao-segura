import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import StackNavigation from './StackNavigation';


const routes = createNativeStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <routes.Navigator>
        <routes.Screen 
          name='Main'
          component={StackNavigation}
          options={{ headerShown: false }}
        />
      </routes.Navigator>
    </NavigationContainer>
  )
}

export default Routes;