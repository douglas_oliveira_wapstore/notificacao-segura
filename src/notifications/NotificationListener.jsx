import { useEffect, useRef } from 'react';
import NotificationService from '../service/NotificationService';
import * as Notifications from 'expo-notifications';
import GenericAlert from '../messages/GenericAlert';
import UserService from '../service/UserService';

function NotificationListener() {
    const responseListener = useRef();

    //Lidando com a notificação PUSH
    useEffect(() => {
        responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
            let hash = response.notification.request.content.data.hash;

            NotificationService.sendMessage(hash).then((response) => {
                GenericAlert("Sucesso!", 'Sua notificação foi disparada com sucesso e os destinatários já estão sendo notificados.');
            })
            .catch((error) => {
                GenericAlert("Erro!", 'Houve um erro na comunicação com os destinatários. Verifique se as informações cadastradas estão corretas.')
            });

            if(UserService.isThereAnyUser()) NotificationService.pushActiveNotificationsExpo();
        });

        return () => {
            Notifications.removeNotificationSubscription(responseListener.current);
        };
    }, []);

    return (null);
}

export default NotificationListener;