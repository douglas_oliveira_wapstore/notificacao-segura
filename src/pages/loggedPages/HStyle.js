import { StyleSheet } from 'react-native';

const HStyle = StyleSheet.create({

    //Estilização da Page Empty

    ContainerEmpty: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },

      ContentEmpty: {
        fontSize: 20,
        margin: 20,
        textAlign: 'center',
        color: '#686868'
    },

    ScrollView: {
        flexGrow: 1,
        justifyContent: 'center',
        backgroundColor: 'white',
    },

    FlashList: {
        backgroundColor: 'white',
        paddingHorizontal: 5,
        paddingBottom: 10,
    },

    ContainerList: {
        flexDirection: 'row',
        borderRadius: 20,
        padding: 20,
        marginTop: 10,
        shadowColor: '#000000',
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
        elevation: 5,
        backgroundColor: 'white',
        height: 150,
    },

    ContainerContent: {
        flex: 1,
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        marginRight: 10,
    },

    ElementsContainer: {
        flexDirection: 'column',
    },

    IconsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 7,
    },

    ButtonContainer: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 1,
    },

    TitleList: {
        color: '#3C9F9F',
        fontWeight: '700',
        fontSize: 22,
        overflow: 'hidden',
    },

    SubtitleList: {
        fontSize: 20,
        fontWeight: '400',
        overflow: 'hidden',
    },

    DescriptionList: {
        flex: 1,
        color: '#686868',
        fontWeight: '300',
        fontSize: 18,
        overflow: 'hidden',
    },

    DefaultImage: {
        width: 250,
        height: 250,
        opacity: 0.5
    },

});

export default HStyle;