import GenericAlert from '../../../messages/GenericAlert';
import React, { useState } from 'react'
import { Text, ScrollView, TextInput, View, Pressable, Image} from 'react-native';
import CStyle from './CStyle';
import GlobalStyles from '../../../assets/styles/GlobalStyles';
import Button from '../../../components/Button';
import TrashIcon from '../../../assets/Icons/TrashIcon';
import UserContact from '../../../assets/Icons/UserContact';
import NotificationService from '../../../service/NotificationService';
import Token from '../../../config/Token';
import { FlashList } from '@shopify/flash-list';
import { v4 as uuidv4 } from 'uuid';

export function AddContact({ navigation, route }) {

    const { notificationData } = route.params;

    //Array com a lista de contatos
    const [contactData, setContactData] = useState({
        phones: []
    });

    //Gerencia os inputs
    const [newContact, setNewContact] = useState({
        id: uuidv4(),
        name: '',
        phone: '',
    })

    //Verifica se há contatos com números iguais
    const isDuplicatePhone = (newPhone) => {
        return contactData.phones && contactData.phones.some((existingContact) => existingContact.phone === newPhone);
    };
  
    //Adiciona os valores pro Array
    const handleAddContact = () => {
      
        if(contactData.phones.length < 10){
            if (newContact.name && newContact.phone && !isDuplicatePhone(newContact.phone)) {
                setContactData({
                    phones: [...contactData.phones, newContact]
                });
                setNewContact({ id: uuidv4(), name: '', phone: '' });
              } else {
                console.log('Preencha corretamente ou telefone duplicado!');
                GenericAlert("Contato Inválido", "Preencha corretamente e certifique-se de não adicionar números duplicados")
              }
        }   
        else{
            GenericAlert('Limite Atingido', 'Você só pode adicionar até 10 contatos');
        }
  
    }
  
    //Remove os contatos do Array
    const removePhone = (id) => {
        setContactData((prevState) => ({
          ...prevState,
          phones: prevState.phones.filter((contact) => contact.id !== id)
        }));
    };

    //Comunicando com o servidor
    const validateRegister = async () => {
        let userToken = await Token();
        const allData = { ...notificationData, ...contactData };
        await NotificationService.create(allData, userToken)
        .catch((error) => {
            console.error(error);
            if(error.response.status === 401){
              GenericAlert('Ação Impedida', 'Faça Login para prosseguir')
              navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }],
              });
            }
            else{
              console.error('Falha na comunicação com servidor', error);
              GenericAlert('Falha ao criar notificação', 'Verifique se digitou dados válidos.');
            }
        });
        GenericAlert("Sucesso!", "Notificação criada :)")
        navigation.navigate('Home');
    }
  
    return(
      <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView}>
        <View style={GlobalStyles.Container}>
          <Text style={CStyle.TitlePage}>Contatos</Text>
  
          <Text style={CStyle.SubtitlePage}>Adicione contatos de sua confiança para esta notificação</Text>
  
          <TextInput
            style={CStyle.InputStyle}
            onChangeText={(text) => setNewContact({ ...newContact, name: text })}
            maxLength={15}
            value={newContact.name}
            placeholder="Nome do contato"
            placeholderTextColor="#A6A6A6"
          />
  
          <TextInput
            style={CStyle.InputStyle}
            onChangeText={(text) => setNewContact({ ...newContact, phone: text })}
            maxLength={14}
            value={newContact.phone}
            placeholder="Telefone"
            placeholderTextColor="#A6A6A6"
          />
          <View style={CStyle.GenericButtonContainer}>
            <Button
              title="Adicionar"
              textColor="white"
              borderRadius={20}
              backgroundColor={"#3C9F9F"}
              width="35%"
              height={45}
              sizeText={18}
              marginTop={5}
              marginBottom={20}
              onPress={() => handleAddContact()}
            />
          </View>
          { contactData.phones.length > 0 ? (
            <View style={CStyle.ContainerListContact}>
                <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView} nestedScrollEnabled={true}>
                    <FlashList
                        contentContainerStyle={CStyle.FlashList}
                        disableAutoLayout={false}
                        data={contactData.phones}
                        keyExtractor={(item) => item.id}
                        renderItem={({ item }) =>
                        <View style={CStyle.ContainerList}>
                            <View style={CStyle.ContainerUserContact}>
                                <UserContact />
                            </View>
                            <View style={CStyle.ContainerContent}>
                                <Text style={CStyle.TitleList}>{item.name}</Text>
                                <Text style={CStyle.SubtitleList}>{item.phone}</Text>
                            </View>
                            <Pressable onPress={() => removePhone(item.id)}>
                                <TrashIcon />
                            </Pressable>
                        </View>
                        }
                        estimatedItemSize={10}
                    />
                </ScrollView>
            </View>
          ) : (
            <View style={CStyle.ContainerContactEmpty}>
                <Image style={CStyle.DefaultImage} source={require('../../../assets/img/megaphoneImage.png')}/>
                <Text style={CStyle.TextEmptyContainer}>Adicione um Contato :)</Text>
            </View>
          )
          }
            <Button
                title="Finalizar"
                textColor="white"
                borderRadius={20}
                backgroundColor={"#3C9F9F"}
                width="75%"
                height={50}
                sizeText={18}
                textTransform="uppercase"
                marginTop={20}
                marginBottom={20}
                onPress={() => validateRegister()}
            />

          </View>
      </ScrollView>
    )
  }
  