import { StyleSheet } from 'react-native';

const CStyle = StyleSheet.create({

  //Estilos Globais

  InputStyle: { // Estilizando os inputs
    margin: 10,
    height: 55,
    fontSize: 20,
    borderRadius: 8,
    backgroundColor: '#EDECEC',
    paddingLeft: 20,
    width: '90%',
  },

  TitlePage: {
    marginTop: 20,
    fontWeight: 'bold',
    fontSize: 28,
    width: '70%',
    textAlign: 'left',
    color: '#3C9F9F',
    width: '90%',
    marginBottom: 20,
  },

  SubtitlePage: {
    fontSize: 20,
    fontWeight: '400',
    overflow: 'hidden',
    color: '#686868',
    width: '90%',
  },

  //Estilizando a Notification Page

  ButtonArrowContainer: {
    width: "90%",
    alignItems: 'flex-end',
  },

  ContainerListContact: {
    flex: 1,
    width: '90%',
    height: 250,
    borderColor: '#3C9F9F',
    borderWidth: 2,
    borderRadius: 7,
    borderStyle: 'solid'
  },

  FlashList: {
    backgroundColor: 'white',
    paddingHorizontal: 5,
    paddingBottom: 10,
  },

  ContainerList: {
    flexDirection: 'row',
    borderRadius: 20,
    padding: 20,
    marginTop: 10,
    shadowColor: '#000000',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 5,
    backgroundColor: 'white',
  },

  ContainerContent: {
    flex: 1,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    marginRight: 10,
  },

  ContainerUserContact: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },

  ContainerContactEmpty: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '90%',
    height: 250,
    borderColor: '#3C9F9F',
    borderWidth: 2,
    borderRadius: 7,
    borderStyle: 'solid'
  },

  TextEmptyContainer: {
    fontSize: 20,
    fontWeight: '400',
    overflow: 'hidden',
    color: '#686868',
    justifyContent: 'center',
  },

  //Estilizando a AddContact Page

  TitleList: {
    color: '#3C9F9F',
    fontWeight: '700',
    fontSize: 22,
    overflow: 'hidden',
  },

  SubtitleList: {
    fontSize: 20,
    fontWeight: '400',
    overflow: 'hidden',
  },

  InputStyleDesc: {
    height: 120,
    textAlignVertical: "top",
    paddingTop: 15,
  },

  DefaultImage: {
    width: 100,
    height: 100,
    opacity: 0.5,
    borderRadius: 125,
  },

  GenericButtonContainer: {
    alignItems: 'flex-start',
    width: '90%',
  },

});

export default CStyle;