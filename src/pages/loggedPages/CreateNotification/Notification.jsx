import React, { useState } from 'react'
import { Text, ScrollView, TextInput, View} from 'react-native';
import GlobalStyles from '../../../assets/styles/GlobalStyles';
import CStyle from './CStyle';
import {Picker} from '@react-native-picker/picker';
import Button from '../../../components/Button';

export function CreateNotification({ navigation }) {

  const emptyObject = {
    icon: 'home',
    type: 'whatsapp',
    app_name: '',
    title: '',
    description: '',
    message:'',
  };

  const [notificationData, setNotificationData] = useState(emptyObject);

  const handleInputChange = (value, name) => {
    setNotificationData({ ...notificationData, [name]: value });
  };
  
  return (
    <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView}>
      <View style={GlobalStyles.Container}>
        <Text style={CStyle.TitlePage}>Criar Notificação</Text>
        <TextInput
          style={CStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'app_name')}
          value={notificationData.app_name}
          maxLength={50}
          placeholder="Nome do aplicativo"
          placeholderTextColor="#A6A6A6"
        />
        <TextInput
          style={CStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'title')}
          value={notificationData.title}
          maxLength={255}
          placeholder="Título da notificação"
          placeholderTextColor="#A6A6A6"
        />
        <TextInput
          style={[CStyle.InputStyle, CStyle.InputStyleDesc]}
          onChangeText={(value) => handleInputChange(value, 'description')}
          value={notificationData.description}
          maxLength={255}
          multiline={true}
          numberOfLines={6}
          placeholder="Descrição"
          placeholderTextColor="#A6A6A6"
        />
        <View style={CStyle.InputStyle}>
          <Picker 
            style={CStyle.InputPicker}
            selectedValue={notificationData.type}
            onValueChange={(value) =>
              handleInputChange(value, 'type')
          }>
            <Picker.Item label="Whatsapp" value="whatsapp" />
            <Picker.Item label="Ligação" value="call" />
          </Picker>
        </View>
        <TextInput
          style={[CStyle.InputStyle, CStyle.InputStyleDesc]}
          onChangeText={(value) => handleInputChange(value, 'message')}
          value={notificationData.message}
          maxLength={255}
          multiline={true}
          numberOfLines={6}
          placeholder="Mensagem de socorro"
          placeholderTextColor="#A6A6A6"
        />

        <View style={CStyle.ButtonArrowContainer}>
          <Button
            title="➜"
            textColor="white"
            borderRadius={100}
            backgroundColor={"#3C9F9F"}
            width={60}
            height={60}
            sizeText={30}
            textTransform="uppercase"
            marginTop={10}
            justifyContent="center"
            alignItems="center"
            onPress={() => {
              navigation.navigate("NotificationContact", {notificationData})
              setNotificationData(emptyObject)
            }}
        />
        </View>
      </View>
    </ScrollView>
  )
}

