import React, { useEffect, useState, useRef } from 'react'
import { Text, View, Image, ScrollView, Switch, Pressable } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import GlobalStyle from '../../assets/styles/GlobalStyles';
import HStyle from '../loggedPages/HStyle';
import HomeIcon from '../../assets/Icons/HomeIcon';
import { FlashList } from '@shopify/flash-list';
import TrashIcon from '../../assets/Icons/TrashIcon';
import UpdateIcon from '../../assets/Icons/UpdateIcon';
import NotificationService from '../../service/NotificationService';
import GenericAlert from '../../messages/GenericAlert';
import Token from '../../config/Token';

function Home({ navigation }) {

  const [dataNotification, setDataNotification] = useState([{}]);
  const isFocused = useIsFocused();


  //Buscando a listagem de usuários
  useEffect(() => {
    NotificationService.pushActiveNotificationsExpo();

    (async function loadNotifications() {
        let userToken = await Token();
        let response = await NotificationService.list(userToken)
        .catch((error) => {
            console.error(error);
            if(error.response.status === 401){
              GenericAlert('Ação Impedida', 'Faça Login para prosseguir')
              navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }],
              });
            }
        });

        setDataNotification(response);
    })();
  }, [isFocused]);

  //Alterando Ativo ou Status da notificação localmente e no servidor
  const alterActive = async (ativo, idNotification) => {
    let userToken = await Token();
    const statusBoolean = {ativo: ativo};
    let response = await NotificationService.on(statusBoolean, userToken, idNotification)
    .catch((error) => {
      GenericAlert("Erro", "Não foi possível alterar o status da notificação");
      console.log("Não foi possível alterar o status da notificação. ", error);
    })
    if(response.status === 200){
      setDataNotification((prevData) => {
        return prevData.map((notification) => {
          if (notification.id === idNotification) {
            return {
              ...notification,
              ativo: ativo,
            };
          }
          return notification;
        });
      });
    }
  }

  //deletando e notificação localmente e no servidor
  const deleteNotification = async (idNotification) => {
    try{
      let userToken = await Token();
      await NotificationService.exclude(userToken, idNotification)
   
      setDataNotification((prevState) =>
        prevState.filter((notification) => notification.id !== idNotification)
      );
    } catch (error) {
      GenericAlert("Erro", "Não foi possível deletar a notificação");
      console.log("Não foi possível deletar a notificação. ", error);
    }
  }

  return (
    <>
      { dataNotification.length === 0 ? (
        <View style={GlobalStyle.Container}>
          <Image style={HStyle.DefaultImage} source={require('../../assets/img/homeImage.png')}/>
          <Text style={HStyle.ContentEmpty}>Ainda não há nenhum Evento, crie um para começarmos!</Text>
          <HomeIcon />
          
        </View>
      ) : (
        <ScrollView contentContainerStyle={HStyle.ScrollView}>
          <FlashList
              contentContainerStyle={HStyle.FlashList}
              disableAutoLayout={false}
              data={dataNotification}
              renderItem={({ item }) =>
                  <View style={HStyle.ContainerList}>
                    <View style={HStyle.ContainerContent}>
                      <Text style={HStyle.TitleList}>{item.app_name}</Text>
                      <Text style={HStyle.SubtitleList}>{item.title}</Text>
                      <Text numberOfLines={2} style={HStyle.DescriptionList}>{item.description}</Text>
                    </View>
                    <View style={HStyle.ElementsContainer}>
                      <View style={HStyle.IconsContainer}>
                        <Pressable onPress={()=> navigation.navigate("EditNotification", item.id)}>
                          <UpdateIcon />
                        </Pressable>
                        <Pressable onPress={()=> deleteNotification(item.id)}>
                          <TrashIcon />
                        </Pressable>               
                      </View>
                      <View style={HStyle.ButtonContainer}>
                        <Switch 
                          trackColor={{ false: "#D9D9D9", true: "#D9D9D9" }}
                          thumbColor={item.ativo ? "#3C9F9F" : "#D9D9D9"}
                          ios_backgroundColor="#D9D9D9"
                          style={{ transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                          onValueChange={() => alterActive(!item.ativo, item.id)}
                          value={item.ativo} />
                      </View>
                    </View>
                  </View>
              }
              estimatedItemSize={200}
          />
        </ScrollView>
      )}
    </> 
  )

}

export default Home;