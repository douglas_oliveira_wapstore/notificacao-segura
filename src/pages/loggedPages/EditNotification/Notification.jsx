import React, { useState, useEffect } from 'react';
import { Text, ScrollView, TextInput, View} from 'react-native';
import GlobalStyles from '../../../assets/styles/GlobalStyles';
import Token from '../../../config/Token';
import EStyle from './EStyle';
import {Picker} from '@react-native-picker/picker';
import Button from '../../../components/Button';
import NotificationService from '../../../service/NotificationService';

export function EditNotification({ navigation, route }) {

  const notificationId = route.params;

  const [notificationData, setNotificationData] = useState({});

  useEffect(() => {
    (async function loadNotifications() {
        let userToken = await Token();
        let response = await NotificationService.findById(notificationId, userToken)
        .catch((error) => {
            console.error(error);
            if(error.response.status === 401){
              GenericAlert('Ação Impedida', 'Faça Login para prosseguir')
              navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }],
              });
            }
        });

        setNotificationData(response);
    })();
  }, []);

  const handleInputChange = (value, name) => {
    setNotificationData({ ...notificationData, [name]: value });
  };
  
  return (
    <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView}>
      <View style={GlobalStyles.Container}>
        <Text style={EStyle.TitlePage}>Editar Notificação</Text>
        <TextInput
          style={EStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'app_name')}
          value={notificationData.app_name}
          maxLength={15}
          placeholder="Nome do aplicativo"
          placeholderTextColor="#A6A6A6"
        />
        <TextInput
          style={EStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'title')}
          value={notificationData.title}
          maxLength={20}
          placeholder="Título da notificação"
          placeholderTextColor="#A6A6A6"
        />
        <TextInput
          style={[EStyle.InputStyle, EStyle.InputStyleDesc]}
          onChangeText={(value) => handleInputChange(value, 'description')}
          value={notificationData.description}
          maxLength={130}
          multiline={true}
          numberOfLines={6}
          placeholder="Descrição"
          placeholderTextColor="#A6A6A6"
        />
        <View style={EStyle.InputStyle}>
          <Picker 
            style={EStyle.InputPicker}
            selectedValue={notificationData.type}
            onValueChange={(value) =>
              handleInputChange(value, 'type')
          }>
            <Picker.Item label="Whatsapp" value="whatsapp" />
            <Picker.Item label="Ligação" value="call" />
          </Picker>
        </View>
        <TextInput
          style={[EStyle.InputStyle, EStyle.InputStyleDesc]}
          onChangeText={(value) => handleInputChange(value, 'message')}
          value={notificationData.message}
          maxLength={130}
          multiline={true}
          numberOfLines={6}
          placeholder="Mensagem de socorro"
          placeholderTextColor="#A6A6A6"
        />

        <View style={EStyle.ButtonArrowContainer}>
          <Button
            title="➜"
            textColor="white"
            borderRadius={100}
            backgroundColor={"#3C9F9F"}
            width={60}
            height={60}
            sizeText={30}
            textTransform="uppercase"
            marginTop={10}
            justifyContent="center"
            alignItems="center"
            onPress={() => navigation.navigate("EditContact", {notificationData})}
        />
        </View>
      </View>
    </ScrollView>
  )
}

