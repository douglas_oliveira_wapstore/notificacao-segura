import GenericAlert from '../../../messages/GenericAlert';
import React, { useState } from 'react'
import { Text, ScrollView, TextInput, View, Pressable, Image} from 'react-native';
import EStyle from './EStyle';
import GlobalStyles from '../../../assets/styles/GlobalStyles';
import Button from '../../../components/Button';
import TrashIcon from '../../../assets/Icons/TrashIcon';
import UserContact from '../../../assets/Icons/UserContact';
import NotificationService from '../../../service/NotificationService';
import Token from '../../../config/Token';
import { FlashList } from '@shopify/flash-list';
import { v4 as uuidv4 } from 'uuid';

export function EditContact({ navigation, route }) {

    const { notificationData } = route.params;

    //Array com a lista de contatos
    const [contactData, setContactData] = useState(notificationData);

    //Gerencia os inputs
    const [newContact, setNewContact] = useState({
        id: uuidv4(),
        name: '',
        phone: '',
    })

    //Verifica se há contatos com números iguais
    const isDuplicatePhone = (newPhone) => {
        return contactData.phones && contactData.phones.some((existingContact) => existingContact.phone === newPhone);
    };

    //Adiciona os valores pro Array
    const handleAddContact = () => {
      
        if(contactData.phones.length < 10){
            if (newContact.name && newContact.phone && !isDuplicatePhone(newContact.phone)) {
                setContactData({
                    phones: [...contactData.phones, newContact]
                });
                setNewContact({ id: uuidv4(), name: '', phone: '' });
              } else {
                console.log('Preencha corretamente ou telefone duplicado!');
                GenericAlert("Contato Inválido", "Preencha corretamente e certifique-se de não adicionar números duplicados")
              }
        }   
        else{
            GenericAlert('Limite Atingido', 'Você só pode adicionar até 10 contatos');
        }
  
    }
  
    //Remove os contatos do Array
    const removePhone = (id) => {
        setContactData((prevState) => ({
          ...prevState,
          phones: prevState.phones.filter((contact) => contact.id !== id)
        }));
    };

    //Comunicando com o servidor
    const validateRegister = async () => {
        let userToken = await Token();
        const allData = {...notificationData, phones: contactData.phones};
        await NotificationService.edit(allData, userToken, allData.id)
        .catch((error) => {
            console.error(error);
            if(error.response.status === 401){
              GenericAlert('Ação Impedida', 'Faça Login para prosseguir')
              navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }],
              });
            }
            else{
              console.error('Falha na comunicação com servidor', error);
              GenericAlert('Falha ao alterar notificação', 'Verifique se digitou dados válidos.');
            }
        });
        GenericAlert("Sucesso!", "Notificação Atualizada :)")
        navigation.navigate('Home');
    }
  
    return(
      <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView}>
        <View style={GlobalStyles.Container}>
          <Text style={EStyle.TitlePage}>Contatos</Text>
  
          <Text style={EStyle.SubtitlePage}>Adicione contatos de sua confiança para esta notificação</Text>
  
          <TextInput
            style={EStyle.InputStyle}
            onChangeText={(text) => setNewContact({ ...newContact, name: text })}
            maxLength={15}
            value={newContact.name}
            placeholder="Nome do contato"
            placeholderTextColor="#A6A6A6"
          />
  
          <TextInput
            style={EStyle.InputStyle}
            onChangeText={(text) => setNewContact({ ...newContact, phone: text })}
            maxLength={14}
            value={newContact.phone}
            placeholder="Telefone"
            placeholderTextColor="#A6A6A6"
          />
          <View style={EStyle.GenericButtonContainer}>
            <Button
              title="Adicionar"
              textColor="white"
              borderRadius={20}
              backgroundColor={"#3C9F9F"}
              width="35%"
              height={45}
              sizeText={18}
              marginTop={5}
              marginBottom={20}
              onPress={() => handleAddContact()}
            />
          </View>
          { contactData.phones.length > 0 ? (
            <View style={EStyle.ContainerListContact}>
                <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView} nestedScrollEnabled={true}>
                    <FlashList
                        contentContainerStyle={EStyle.FlashList}
                        disableAutoLayout={false}
                        data={contactData.phones}
                        keyExtractor={(item) => item.id}
                        renderItem={({ item }) =>
                        <View style={EStyle.ContainerList}>
                            <View style={EStyle.ContainerUserContact}>
                                <UserContact />
                            </View>
                            <View style={EStyle.ContainerContent}>
                                <Text style={EStyle.TitleList}>{item.name}</Text>
                                <Text style={EStyle.SubtitleList}>{item.phone}</Text>
                            </View>
                            <Pressable onPress={() => removePhone(item.id)}>
                                <TrashIcon />
                            </Pressable>
                        </View>
                        }
                        estimatedItemSize={10}
                    />
                </ScrollView>
            </View>
          ) : (
            <View style={EStyle.ContainerContactEmpty}>
                <Image style={EStyle.DefaultImage} source={require('../../../assets/img/megaphoneImage.png')}/>
                <Text style={EStyle.TextEmptyContainer}>Adicione um Contato :)</Text>
            </View>
          )
          }
            <Button
                title="Finalizar"
                textColor="white"
                borderRadius={20}
                backgroundColor={"#3C9F9F"}
                width="75%"
                height={50}
                sizeText={18}
                textTransform="uppercase"
                marginTop={20}
                marginBottom={20}
                onPress={() => validateRegister()}
            />

          </View>
      </ScrollView>
    )
  }
  