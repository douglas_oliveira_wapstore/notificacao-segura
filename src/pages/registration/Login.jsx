import React, { useState } from 'react';
import { View, Text, Pressable, TextInput, ScrollView } from 'react-native';
import Button from '../../components/Button';
import RStyle from './RStyle';
import GlobalStyles from '../../assets/styles/GlobalStyles';
import UserLogin from '../../assets/Icons/UserLogin';
import AsyncStorage from '@react-native-async-storage/async-storage';
import UserService from '../../service/UserService';
import GenericAlert from '../../messages/GenericAlert';

function Login({ navigation }) {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const userData = { email: email, password: password };

  const validateLogin = async () => {
    try {
      let token = await UserService.login(userData)
        if (token) {
          await AsyncStorage.setItem('userToken', token)
          .catch((error) => {
            console.error('Falha ao armazenar token', error);
          })
          navigation.reset({
            index: 0,
            routes: [{ name: 'HomeTabNav' }],
          });
        }
    } catch (error) {
      console.log("Não foi possível realizar Login", error);
      GenericAlert('Falha no Login', 'Digite um email ou senha válidos.');
    }
  };

  return (
    <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView}>
      <View style={GlobalStyles.Container}>

        <UserLogin style={RStyle.DefaultIcon}/>

        <Text style={RStyle.TitlePage}>Login</Text>

        <TextInput
          style={RStyle.InputStyle}
          onChangeText={setEmail}
          maxLength={64}
          value={email}
          placeholder="Email"
          placeholderTextColor="#A6A6A6"
        />

        <TextInput
          style={RStyle.InputStyle}
          onChangeText={setPassword}
          secureTextEntry={true}
          value={password}
          maxLength={64}
          placeholder="Senha"
          placeholderTextColor="#A6A6A6"
        />

        <Pressable style={RStyle.ForgotPassword}>
          <Text style={RStyle.ForgotPasswordText}>
            Esqueci minha senha
          </Text>
        </Pressable>

        <Button
            title="Login"
            textColor="white"
            borderRadius={20}
            backgroundColor={"#3C9F9F"}
            width="75%"
            height={50}
            sizeText={18}
            textTransform="uppercase"
            marginTop={55}
            marginBottom={20}
            onPress={() => validateLogin()}
        />

        <View style={RStyle.TabButtonContainer}>
          <View style={RStyle.TabButton}>
            <Pressable onPress={() => navigation.navigate('Register')}>
              <Text style={RStyle.TabButtonTitle}>Criar Conta</Text>
            </Pressable>
          </View>
        </View>

      </View>
    </ScrollView>
  )
}

export default Login;