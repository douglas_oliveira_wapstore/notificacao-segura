import React from 'react'
import { Text, Image, ScrollView } from 'react-native';
import RStyle from '../registration/RStyle';
import { LinearGradient } from 'expo-linear-gradient';
import Button from '../../components/Button';

function Start({ navigation }) {
  return (
    <LinearGradient colors={['#EFF5F5', '#8DC5C5', '#347B7B']} style={RStyle.ContainerStart}>
      <ScrollView contentContainerStyle={RStyle.ContainerStart}>

          <Image style={RStyle.DefaultImage} source={require('../../assets/img/startImage.png')}/>

          <Text style={RStyle.Title}>Bem-Vindo ao Notificação Segura</Text>
          <Text style={RStyle.Content}>
              Confie em nós para lidar com as coisas nos bastidores,
              basta configurar sua notificação e o resto está em nossas
              mãos.
          </Text>

          <Button
            title="Começar"
            textColor="black"
            borderRadius={20}
            backgroundColor={"#D6D6D6"}
            width={240}
            height={45}
            sizeText={18}
            marginTop={50}
            marginBottom={20}
            onPress={() => navigation.navigate('Login')}
          />
      </ScrollView>
    </LinearGradient>
  )
}

export default Start;