import React, { useState } from 'react'
import { View, Text, Pressable, TextInput, ScrollView } from 'react-native';
import Button from '../../components/Button';
import RStyle from './RStyle';
import GlobalStyles from '../../assets/styles/GlobalStyles';
import UserService from '../../service/UserService';
import GenericAlert from '../../messages/GenericAlert';

function Register({ navigation }) {

  const [userData, setUserData] = useState({
    name: '',
    email: '',
    phone: '',
    password: '',
    password_confirmation: '',
  }); 

  const handleInputChange = (value, name) => {
      setUserData({ ...userData, [name]: value });
  };

  const validateRegister = async () => {
    try {
      await UserService.create(userData);
      navigation.navigate('Login');
    } catch (error) {
      console.error('Falha na comunicação com servidor', error);
      GenericAlert('Falha ao criar conta', 'Digite dados válidos.');
    }
  }

  return (
    <ScrollView contentContainerStyle={GlobalStyles.ContainerScrollView}>
      <View style={GlobalStyles.Container}>

        <View style={RStyle.TitleContainer}>
          <Text style={RStyle.TitlePage}>Criar Conta</Text>
        </View>

        <TextInput
          style={RStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'name')}
          maxLength={64}
          value={userData.name}
          placeholder="Nome"
          placeholderTextColor="#A6A6A6"
        />
        <TextInput
          style={RStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'email')}
          maxLength={64}
          value={userData.email}
          placeholder="Email"
          placeholderTextColor="#A6A6A6"
        />
        <TextInput
          style={RStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'phone')}
          maxLength={64}
          value={userData.phone}
          placeholder="Telefone"
          placeholderTextColor="#A6A6A6"
        />

        <TextInput
          style={RStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'password')}
          secureTextEntry={true}
          value={userData.password}
          maxLength={64}
          placeholder="Senha"
          placeholderTextColor="#A6A6A6"
        />

        <TextInput
          style={RStyle.InputStyle}
          onChangeText={(value) => handleInputChange(value, 'password_confirmation')}
          secureTextEntry={true}
          value={userData.password_confirmation}
          maxLength={64}
          placeholder="Confirmar Senha"
          placeholderTextColor="#A6A6A6"
        />

        <Button
            title="Cadastrar"
            textColor="white"
            borderRadius={20}
            backgroundColor={"#3C9F9F"}
            width="75%"
            height={50}
            sizeText={18}
            textTransform="uppercase"
            marginTop={55}
            marginBottom={20}
            onPress={() => validateRegister()}
        />

        <View style={RStyle.TabButtonContainer}>
          <View style={RStyle.TabButton}>
            <Pressable onPress={() => navigation.navigate('Login')}>
              <Text style={RStyle.TabButtonTitle}>Realizar Login</Text>
            </Pressable>
          </View>
        </View>

      </View>
    </ScrollView>
  )
}

export default Register;