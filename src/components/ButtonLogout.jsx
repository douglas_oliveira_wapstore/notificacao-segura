import React from 'react'
import { Pressable } from 'react-native'
import LogoutIcon from '../assets/Icons/LogoutIcon';

function ButtonLogout(props) {
  return (
    <Pressable onPress={props.onPress}>
        <LogoutIcon />
    </Pressable>
  )
}

export default ButtonLogout;