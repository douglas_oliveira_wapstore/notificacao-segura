import React from 'react'
import { Pressable, Text } from 'react-native';

function Button({
    title, 
    textColor, 
    borderRadius,
    backgroundColor, 
    width, 
    height, 
    marginTop, 
    marginBottom,
    sizeText,
    textTransform,
    onPress}) 
    {

  return (
    <Pressable
        onPress={onPress}
        style={{
            backgroundColor: backgroundColor,
            width: width,
            height: height,
            borderRadius: borderRadius || 5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: marginTop,
            marginBottom: marginBottom,
        }}>
        <Text 
            style={{
                color: textColor,
                fontSize: sizeText,
                textTransform: textTransform,
                fontWeight: 'bold',
            }}>
            {title}
        </Text>
    </Pressable>
  )
}

export default Button